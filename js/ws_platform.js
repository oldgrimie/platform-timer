//PLATFORM CLIENT
var WS = null;
var WS_PORT = 6789;
var WS_LOCATION = window.location.origin.replace('https:', 'ws:').replace('http:', 'ws:');
var DATA = {
    status: 'stop',
    minutes: 0,
    seconds: 0,
    hoursLimit: 0,
    minutesLimit: 30,
    startUTC: new Date().toUTCString(),
    message: ''
};

var timer = new Timer();


function onOpenWS(ev) {
    console.log('SOCKET CONNECTED');
}

function onCloseWS(ev) {
    console.log('SOCKET CLOSED');
    initSocket();
}

function playTimer(data) {
    if (data.startUTC == DATA.startUTC) {
        // Case of resume pause
        timer.start({
            precision: 'seconds',
            startValues: { minutes: data.minutes, seconds: data.seconds }
        });
        console.log('RESUMED');
        return;
    }
    // Case of connecting while running
    let localDate = new Date();
    let remoteDate = new Date(Date.parse(data.startUTC));
    let diffDate = new Date(localDate.getTime() - remoteDate.getTime());
    timer.start({
        precision: 'seconds',
        startValues: { minutes: diffDate.getUTCMinutes(), seconds: diffDate.getUTCSeconds() }
    });
    console.log('STARTED');
}

function pauseTimer(data) {
    const zeroPad = (num, places) => String(num).padStart(places, '0');
    timer.pause();
    $('#clock').html(`${zeroPad(data.minutes, 2)}:${zeroPad(data.seconds, 2)}`);
    console.log('PAUSED');
}

function stopTimer() {
    timer.stop();
    $('#clock').html('00:00');
    console.log('STOPPED');
}

function loadData(json_str) {
    const timerMethods = {
        'play': playTimer,
        'pause': pauseTimer,
        'stop': stopTimer
    };
    let data = JSON.parse(json_str);
    timerMethods[data.status](data);
    $("#messageDisplay").text(data.message);
    DATA = data;
    minutesUpdate();
}

function onMessageWS(ev) {
    loadData(ev.data);
}

function secondsUpdate(ev) {
    $('#clock').html(timer.getTimeValues().toString(['minutes', 'seconds']));
}

function minutesUpdate(ev) {
    let timerValues = timer.getTimeValues();
    let lastMinute = timerValues.hours == DATA.hoursLimit && timerValues.minutes == (DATA.minutesLimit - 1)
    if (lastMinute) {
        $("#clock").removeClass("clock-default");
        $("#clock").removeClass("clock-danger");
        $("#clock").addClass("clock-warning");
        return;
    }
    let passedHours = timerValues.hours > DATA.hoursLimit;
    let sameHours = timerValues.hours == DATA.hoursLimit;
    let passedMinutes = timerValues.minutes >= DATA.minutesLimit;
    let overTime = passedHours || (sameHours && passedMinutes)
    if (overTime) {
        $("#clock").removeClass("clock-default");
        $("#clock").removeClass("clock-warning");
        $("#clock").addClass("clock-danger");
        return;
    }
    $("#clock").removeClass("clock-warning");
    $("#clock").removeClass("clock-danger");
    $("#clock").addClass("clock-default");
}

function initSocket() {
    let location = WS_LOCATION.split(':');
    WS = new WebSocket(`${location[0]}:${location[1]}:${WS_PORT}`);
    WS.onopen = onOpenWS;
    WS.onmessage = onMessageWS;
    WS.onclose = onCloseWS;
    // Update clock's text every second
    timer.addEventListener('secondsUpdated', secondsUpdate);
    timer.addEventListener('minutesUpdated', minutesUpdate);
}
function sendPing() {
    console.log('PING');
    WS.send('PING');
}

$(function () {
    try {
        initSocket();
    } catch {
        console.log('service not found, starting');
    }
    setInterval(sendPing, 60000);
});

