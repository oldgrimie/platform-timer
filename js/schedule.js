


function loadProgram() {
    clearTable();
    $.post('php/get_schedule.php', {}, function (data) {
        jsonObject = JSON.parse(data);
        $.each(jsonObject, function (i, item) {
            selectTiempo = getSelectTiempo(item.duracion);
            $("#tblPrograma tbody").append(
                '<tr class="itemRow">' +
                '<td><input class="form-control input-sm text-right nro" name="nro" placeholder="Bosquejo" type="text" value="' + item.nro + '"/></td>' +
                '<td>' + selectTiempo + '</td>' +
                '<td><input class="form-control input-sm orador" name="orador" placeholder="Orador" type="text" value="' + item.orador + '"/></td>' +
                '<td><input class="form-control input-sm tema" name="tema" placeholder="Tema" type="text" value="' + item.tema + '"/></td>' +
                // '<td><button name="btn_load" class="btn btn-primary">Select</button></td>' +
                '</tr>'
            );
        });


    });
}
function clearTable(){
    $("tr.itemRow").remove();
}

loadProgram();



function getSelectTiempo(duracion) {
    options = '';
    for (minuto = 0; minuto < 60; minuto++) {
        selected = '';
        if (parseInt(duracion) === minuto) {

            selected = 'selected="selected"';
        }
        options += '<option value="' + minuto + '" ' + selected + '>' + minuto + ' min.</option>';
    }
    return '<select class="form-control input-sm duracion" name="duracion" >' +
        options + '</select>';
}
$("#saveRecords").click(function () {
    sendRows();
});
$("#deleteRecords").click(function () {
    deleteRows();
});

var listRows = [];

function sendRows() {
    listRows = [];
    getListRows();
    saveRecords(listRows);
}

function deleteRows() {
    if (confirm('Confirme antes de eliminar todos los discursos')) {
        listRows = [];
        saveRecords();
    }
}

function getListRows() {
    //listRows = $("#tblPrograma tr.itemRow");
    $("#tblPrograma tr.itemRow").each(function (index, element) {
        var currentIndex = $(".itemRow").index(this);
        row = {
            nro: $('.nro:eq(' + index + ')').val(),
            duracion: $('.duracion:eq(' + index + ')').val(),
            orador: $('.orador:eq(' + index + ')').val(),
            tema: $('.tema:eq(' + index + ')').val()
        }
        listRows.push(row);
        //console.log(element);
    });
}

function saveRecords(listRows) {
    $.post('php/save_schedule.php', { rows: listRows }, function (data) {
        loadProgram();
    });
}

$("#addRow").click(function () {
    selectTiempo = getSelectTiempo(0);
    $("#tblPrograma tbody").append(
        '<tr class="itemRow">' +
        '<td><input class="form-control input-sm text-right nro" name="nro" placeholder="Bosquejo" type="text" value=""/></td>' +
        '<td>' + selectTiempo + '</td>' +
        '<td><input class="form-control input-sm orador" name="orador" placeholder="Orador" type="text" value=""/></td>' +
        '<td><input class="form-control input-sm tema" name="tema" placeholder="Tema" type="text" value=""/></td>' +
        // '<td><button name="btn_load" class="btn btn-primary">Select</button></td>' +
        '</tr>'
    );

});