//CRONOMETRO
var timer = new Timer();
var minuteAlert = 0;
var totalMinutes = 0;
var pastMinutes = 0;

function after_load_status(data) {
    if (data.sync) {
        pastMinutes = data.sync.minutes
    }
    minuteAlert = parseInt(data.duration) - 1;
    if (data.status == 'stop') {
        pastMinutes = 0;
        $('#clock').css({ 'color': 'white', 'background': 'black' });
    } else {
        if (pastMinutes >= minuteAlert) {
            // On last minute
            if (minuteAlert == pastMinutes) {
                $('#clock').css({ 'color': 'black', 'background': 'yellow' });
            }
            // Overtime
            if (minuteAlert < pastMinutes) {
                $('#clock').css({ 'color': 'white', 'background': 'red' });
            }
        } else {
            $('#clock').css({ 'color': 'white', 'background': 'black' });
        }
    }

}
$(function () {

    init_socket();

    // Update clock's text every second
    timer.addEventListener('secondsUpdated', function (e) {
        $('#clock').text(timer.getTimeValues().toString(['minutes', 'seconds']));
        //$('#clock').text(timer.callbackTimer().toString(['minutes','seconds']));
    });
    timer.addEventListener('minutesUpdated', function (e) {
        pastMinutes = timer.getTimeValues().minutes;
        if (pastMinutes >= minuteAlert) {
            // On last minute
            if (minuteAlert == pastMinutes) {
                $('#clock').css({ 'color': '#000000', 'background': '#f6fa09' });
            }
            // Overtime
            if (minuteAlert < pastMinutes) {
                $('#clock').css({ 'color': '#FFFFFF', 'background': '#f42121' });
            }
        } else {
            $('#clock').css({ 'color': 'white', 'background': 'black' });
        }

    });
    timer.addEventListener('started', function (e) {
        $('#clock').text(timer.getTimeValues().toString(['minutes', 'seconds']));
    });
    syncTime();
});
