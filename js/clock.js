
var server_timestamp = ''; // server UTC
var client_timestamp = ''; // browser timezone
var server_date = '';
var timer = new Timer();
var clock_status = 'stop';
var clock_duration = 0;
var ws = null;
function init_socket() {
    websocket_uri = 'ws://' + window.location.host + ':9000'
    ws = new WebSocket(websocket_uri);
    ws.onopen = function (ev) { console.log('SOCKET CONNECTED') };
    ws.onmessage = function (ev) { load_status(ev.data) };
    ws.onclose = function (ev) { console.log('SOCKET CLOSED') };
}
function convertUTCDateToLocalDate(date) {
    var newDate = new Date(date.getTime() + date.getTimezoneOffset() * 60 * 1000);
    var offset = date.getTimezoneOffset() / 60;
    var hours = date.getHours();
    newDate.setHours(hours - offset);
    return newDate;
}
function load_status(data_string) {
    data = JSON.parse(data_string);
    //Custom code to update clock status
    //server_date = convertUTCDateToLocalDate(new Date(data.timestamp));
    server_timestamp = data.timestamp;
    totalMinutes = parseInt(data.duration);
    startMinutes = 0;
    startSeconds = 0;
    console.log(data_string);
    if (data.status != clock_status) {
        // When socket detects a different status
        switch (data.status) {
            case 'stop': {
                timer.stop();
                $('#clock').text('00:00');
                break;
            }
            case 'pause': {
                timer.pause();
                break;
            }
            case 'running': {
                if (data.sync) {
                    // Sync with server in case of refresh page
                    startMinutes = data.sync.minutes;
                    startSeconds = data.sync.seconds;
                    timer.start({ precision: 'seconds', startValues: { minutes: startMinutes, seconds: startSeconds } });
                } else {
                    timer.start();
                }
                break;
            }
            default:
                timer.stop();
                break;
        }

    }
    $('#message').text(data.message);
    clock_status = data.status;
    clock_duration = parseInt(data.duration);
    if (typeof (after_load_status) === 'function') {
        after_load_status(data);
    }
}
function sendMessage() {
    formdata = { message: $('#inputMessage').val() };
    $.post('php/post_message.php', formdata, function (data) { ws.send(data); load_status(data); });
}

function syncTime() {
    $.get('php/status.php', function (data) {
        load_status(data);
    });
}
/*
$(function () {
    init_socket();
    $('#btnPlay').click(function () {
        $.post('php/play.php', null, function (data) { ws.send(data); load_status(data); });
    });
    $('#btnPause').click(function () {
        $.post('php/pause.php', null, function (data) { ws.send(data); load_status(data); });
    });
    $('#btnStop').click(function () {
        $.post('php/stop.php', null, function (data) { ws.send(data); load_status(data); });
    });
    $('#btnMessage').click(function () { sendMessage() });
    $('#inputMessage').on('keyup', function (e) { if (e.which == 13) { sendMessage(); } })
    // Update clock's text every second
    timer.addEventListener('secondsUpdated', function (e) {
        $('#clock').text(timer.getTimeValues().toString(['minutes', 'seconds']));
        //$('#clock').text(timer.callbackTimer().toString(['minutes','seconds']));
    });
});
*/
