var currentRow = -1;
var jsonPrograma = [];

function remark_row(rowIndex) {
	$('.itemRow:eq(' + currentRow + ')').removeClass('info');
	$('.itemRow:eq(' + currentRow + ')').removeClass('text-primary');
	$('.itemRow:eq(' + rowIndex + ')').addClass('info');
	$('.itemRow:eq(' + rowIndex + ')').addClass('text-primary');
	currentRow = rowIndex;
}
//CRONOMETRO
var GLOBAL_TIMEOUT = 2000;
var timer = new Timer();
var minuteAlert = 0;
var startMinutes = 0;
var startSeconds = 0;
var totalMinutes = 0;
var pastMinutes = 0;



function getSelectTiempo(duracion) {
	options = '';
	for (minuto = 0; minuto < 60; minuto++) {
		selected = '';
		if (parseInt(duracion) === minuto) {

			selected = 'selected="selected"';
		}
		options += '<option value="' + minuto + '" ' + selected + '>' + minuto + ' min.</option>';
	}
	return '<select class="form-control input-sm duracion" style="width:85px;" name="duracion" >' +
		options + '</select>';
}

function timer_start() {
	formdata = { duration: totalMinutes }
	$.post('php/play.php', formdata, function (data) {
		ws.send(data);
		load_status(data);
		json = JSON.parse(data);
		minuteAlert = parseInt(json.duration) - 1;
	});
}
function timer_stop() {
	formdata = { duration: totalMinutes }
	pastMinutes = 0;
	$.post('php/stop.php', formdata, function (data) {
		ws.send(data);
		load_status(data);
	});
}
function timer_pause() {
	formdata = { duration: totalMinutes }
	$.post('php/pause.php', formdata, function (data) {
		ws.send(data);
		load_status(data);
	});
}
function timer_message() {
	formdata = { message: $('#inputMessage').val(), duration: totalMinutes };
	$.post('php/post_message.php', formdata, function (data) {
		ws.send(data);
		load_status(data);
		json = JSON.parse(data);
		minuteAlert = parseInt(json.duration) - 1;
	});
}

function after_load_status(data) {
	minuteAlert = clock_duration - 1;
	if (pastMinutes >= minuteAlert) {
		// On last minute
		if (minuteAlert == pastMinutes) {
			$('#clock').css({ 'color': '#000000', 'background': '#f6fa09' });
		}
		// Overtime
		if (minuteAlert < pastMinutes) {
			$('#clock').css({ 'color': '#FFFFFF', 'background': '#f42121' });
		}
	} else {
		$('#clock').css({ 'color': 'white', 'background': 'black' });
	}
	if (clock_status == 'stop') {
		pastMinutes = 0;
		$('#clock').css({ 'color': 'white', 'background': 'black' });
		$('#btnPlay').prop('disabled', false);
		$('#btnPause').prop('disabled', true);
		$('#btnStop').prop('disabled', true);
	}
	if (clock_status == 'pause') {
		$('#btnPlay').prop('disabled', false);
		$('#btnPause').prop('disabled', true);
		$('#btnStop').prop('disabled', false);
	}
	if (clock_status == 'running') {
		$('#btnPlay').prop('disabled', true);
		$('#btnPause').prop('disabled', false);
		$('#btnStop').prop('disabled', false);
	}

}

$(function () {
	init_socket();
	$('#btnPlay').click(timer_start);
	$('#btnPause').click(timer_pause);
	$('#btnStop').click(timer_stop);
	$('#btnMessage').click(timer_message());
	$('#inputMessage').on('keyup', function (e) {
		if (e.which == 13) {
			timer_message();
		}
	});
	$('#clearMessage').click(function () {
		$('#inputMessage').val(' ');
		timer_message();
	});
	// Update clock's text every second
	timer.addEventListener('secondsUpdated', function (e) {
		$('#clock').text(timer.getTimeValues().toString(['minutes', 'seconds']));
	});
	// Update clock's text every minute
	timer.addEventListener('minutesUpdated', function (e) {
		pastMinutes = timer.getTimeValues().minutes;
		if (pastMinutes >= minuteAlert) {
			// On last minute
			if (minuteAlert == pastMinutes) {
				$('#clock').css({ 'color': '#000000', 'background': '#f6fa09' });
			}
			// Overtime
			if (minuteAlert < pastMinutes) {
				$('#clock').css({ 'color': '#FFFFFF', 'background': '#f42121' });
			}
		} else {
			$('#clock').css({ 'color': 'white', 'background': 'black' });
		}
	});

	$('#btnPlay').prop('disabled', true);
	$('#btnPause').prop('disabled', true);
	$('#btnStop').prop('disabled', true);

	//LISTA DISCURSOS
	$.post('php/get_schedule.php', {}, function (data) {
		jsonPrograma = JSON.parse(data);
		for (current = 0; current < jsonPrograma.length; current++) {
			var discurso = jsonPrograma[current];
			selectTiempo = getSelectTiempo(discurso.duracion);
			$("#tblDiscursos tbody").append(
				'<tr class="itemRow overlay" style="cursor:pointer">' +
				'<td><input class="nro" name="nro" type="hidden" value="' + discurso.nro + '"/>Bosquejo ' + discurso.nro + selectTiempo + '</td>' +
				'<td><input class="orador" name="orador" type="hidden" value="' + discurso.orador + '"/>' + discurso.orador + '</td>' +
				'<td><input class="tema" name="tema" type="hidden" value="' + discurso.tema + '"/>' + discurso.tema + '</td>' +
				'<td><input type="button" id="' + current + '" value="Cargar" class="btn btn-primary selector"/></td>' +
				'</tr>'
			);
		}
		//Para boton Cargar
		$(".selector").click(function () {
			//Obtener index del boton
			currentIndex = $(this)[0].id;
			var nro = $('.nro:eq(' + currentIndex + ')').val();
			var duracion = $('.duracion:eq(' + currentIndex + ')').val();
			var orador = $('.orador:eq(' + currentIndex + ')').val();
			var tema = $('.tema:eq(' + currentIndex + ')').val();
			var sesion = $('.sesion:eq(' + currentIndex + ')').val();
			//reiniciar valores de tiempo
			minuteAlert = 0;
			startMinutes = 0;
			startSeconds = 0;
			totalMinutes = parseInt(duracion);
			if (currentIndex == currentRow) {
				timer_message();
			} else {
				timer_stop();
			}
			//Establecer discurso en panel administrador
			$('#lblTema').text(nro + '.- ' + tema + ' - ' + orador + ' (' + duracion + ' Min.)');
			$('#ctrlDuracion').val(parseInt(duracion) - 1);
			minuteAlert = parseInt(duracion) - 1;
			$('#mensaje').val('');
			remark_row(currentIndex);
		});

	});
});

