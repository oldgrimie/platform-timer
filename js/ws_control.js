// ADMINISTRATION CLIENT
var WS = null;
var WS_PORT = 6789;
var WS_LOCATION = window.location.origin.replace('https:', 'ws:').replace('http:', 'ws:');
var DATA = {
    status: 'stop',
    hours: 0,
    minutes: 0,
    seconds: 0,
    hoursLimit: 0,
    minutesLimit: 30,
    startUTC: new Date().toUTCString(),
    message: '',
    history: []
};

var timer = new Timer();

function secondsUpdate(ev) {
    $('#clock').html(timer.getTimeValues().toString(['minutes', 'seconds']));
    $('#inputDuracion').html(timer.getTimeValues().toString(['minutes', 'seconds']));
}

function minutesUpdate(ev) {
    let timerValues = timer.getTimeValues();
    if (
        (timerValues.hours == DATA.hoursLimit)
        &&
        (timerValues.minutes == (DATA.minutesLimit - 1))
    ) {
        $("#clock").removeClass("clock-default");
        $("#clock").removeClass("clock-danger");
        $("#clock").addClass("clock-warning");
        return;
    }
    if (
        (timerValues.hours > DATA.hoursLimit)
        ||
        (timerValues.hours == DATA.hoursLimit && timerValues.minutes >= DATA.minutesLimit)
    ) {
        $("#clock").removeClass("clock-default");
        $("#clock").removeClass("clock-warning");
        $("#clock").addClass("clock-danger");
        return;
    }
    $("#clock").removeClass("clock-warning");
    $("#clock").removeClass("clock-danger");
    $("#clock").addClass("clock-default");
}

function initSocket() {
    let location = WS_LOCATION.split(':');
    WS = new WebSocket(`${location[0]}:${location[1]}:${WS_PORT}`);
    WS.onopen = onOpenWS;
    WS.onmessage = onMessageWS;
    WS.onclose = onCloseWS;
    // Update clock's text every second
    timer.addEventListener('secondsUpdated', secondsUpdate);
    timer.addEventListener('minutesUpdated', minutesUpdate);
}

function onOpenWS(ev) {
    console.log('SOCKET CONNECTED');
}

function onCloseWS(ev) {
    console.log('SOCKET CLOSED');
    initSocket();
}

function onMessageWS(ev) {
    if (ev === undefined) return;
    // Validate current data
    loadData(ev.data);
    console.log(ev.data);
}

function playTimer(data) {
    if (data.startUTC == DATA.startUTC) {
        // Case of resume pause
        timer.start({
            precision: 'seconds',
            startValues: { minutes: data.minutes, seconds: data.seconds }
        });
        console.log('RESUMED');
        return;
    }
    // Case of connecting while running
    let localDate = new Date();
    let remoteDate = new Date(Date.parse(data.startUTC));
    let diffDate = new Date(localDate.getTime() - remoteDate.getTime());
    timer.start({
        precision: 'seconds',
        startValues: {
            minutes: diffDate.getUTCMinutes(),
            seconds: diffDate.getUTCSeconds()
        }
    });
    console.log('STARTED');
}
function pauseTimer(data) {
    const zeroPad = (num, places) => String(num).padStart(places, '0');
    timer.pause();
    $('#clock').html(`${zeroPad(data.minutes, 2)}:${zeroPad(data.seconds, 2)}`);
    console.log('PAUSED');
}
function stopTimer() {
    timer.stop();
    $('#clock').html('00:00');
    console.log('STOPPED');
}

function loadData(json_str) {
    // const timerMethods = {
    //     'play': playTimer,
    //     'pause': pauseTimer,
    //     'stop': stopTimer
    // };
    // timerMethods[data.status](data);
    let data = JSON.parse(json_str);
    switch (data.status) {
        case "play":
            playTimer(data);
            break;
        case "pause":
            pauseTimer(data);
            break;
        case "stop":
            stopTimer(data);
            break;

    }
    $("#messageDisplay").text(data.message);
    let totalMinutes = (data.hoursLimit * 60) + data.minutesLimit;
    $("#limitDisplay").text(totalMinutes);
    $("#inputLimits").html(totalMinutes.toString());
    DATA = data;
    minutesUpdate(null);
    loadHistory();
}
function loadHistory() {
    $("tr.history").remove();
    var historyBody = $("#historyBody");
    for (let row of DATA.history) {
        historyBody.append(`
        <tr class="history">
            <td>${row.nro}</td>
            <td>${row.limite}</td>
            <td>${row.duracion}</td>
            <td>${row.orador}</td>
            <td>${row.tema}</td>
            <td></td>
        </tr>`);
    }
}


function sendData() {
    WS.send(JSON.stringify(DATA));
}

function clickPlay(ev) {
    const dt = new Date();
    let timeValues = timer.getTimeValues();
    DATA["status"] = 'play';
    DATA["hours"] = timeValues.hours;
    DATA["minutes"] = timeValues.minutes;
    DATA["seconds"] = timeValues.seconds;
    DATA["startUTC"] = DATA.status == 'stop' ? dt.toUTCString() : DATA.startUTC;
    DATA["message"] = DATA.message;
    DATA["minutesLimit"] = DATA.minutesLimit;
    DATA["hoursLimit"] = DATA.hoursLimit;
    sendData();
}

function clickPause(ev) {
    let timeValues = timer.getTimeValues();
    DATA["status"] = 'pause';
    DATA["hours"] = timeValues.hours;
    DATA["minutes"] = timeValues.minutes;
    DATA["seconds"] = timeValues.seconds;
    DATA["startUTC"] = DATA.startUTC;
    DATA["message"] = DATA.message;
    DATA["minutesLimit"] = DATA.minutesLimit;
    DATA["hoursLimit"] = DATA.hoursLimit;
    sendData();
}

function clickSend(ev) {
    DATA["message"] = $("#inputMessage").val();
    let totalMinutes = parseInt($("#inputLimit").val());
    let minutesLimit = totalMinutes % 60;
    DATA["minutesLimit"] = minutesLimit;
    DATA["hoursLimit"] = (totalMinutes - minutesLimit) / 60;
    sendData();
}

function clickClear(ev) {
    $("#inputMessage").val("");
}
function clickSave(ev) {
    const zeroPad = (num, places) => String(num).padStart(places, '0');
    let history = DATA.history ?? [];
    // let timerValues = timer.getTimeValues();
    // let timeStrings = [];
    // if (timerValues.hours > 0) timeStrings.push(timerValues.hours.toString());
    // timeStrings.push(zeroPad(timerValues.minutes, 2));
    // timeStrings.push(zeroPad(timerValues.seconds, 2));
    history.push({
        nro: $("#inputNro").val(),
        // asignado: `${$("#inputAsignado").val()}:00`,
        limite: $("#inputLimits").html() + ' min.',
        duracion: $("#inputDuracion").html(),
        orador: $("#inputOrador").val(),
        tema: $("#inputTema").val(),
    });
    DATA["history"] = history;
    sendData();
}

function clickDelete(ev) {
    DATA["history"] = [];
    sendData();
}

function clickStop(ev) {
    DATA["status"] = 'stop';
    DATA["hours"] = 0;
    DATA["minutes"] = 0;
    DATA["seconds"] = 0;
    DATA["startUTC"] = '';
    DATA["message"] = DATA.message;
    DATA["minutesLimit"] = DATA.minutesLimit;
    DATA["hoursLimit"] = DATA.hoursLimit;
    sendData();
}
function clickExport(ev) {

    let title = "RESUMEN CRONOMETRO";
    var table_text = "<table>";
    table_text += `<tr><td colspan="6" style="text-align:center;font-weight:bold;">${title}</td></tr>`;
    table_text += `<tr>
        <th>Nro.</th>
        <th>Limite</th>
        <th>Duracion</th>
        <th>Orador</th>
        <th>Tema</th>
        <th></th>
        </tr>`;

    let lines = [];
    var rows = $("tr.history");
    for (let row of rows) {
        lines.push("<tr>" + row.innerHTML + "</tr>");
    }
    table_text += lines.join("\n");
    table_text += "</table>";
    var downloadLink = document.createElement("a");
    downloadLink.href = 'data:application/vnd.ms-excel;base64,' + window.btoa(table_text);
    downloadLink.download = 'discursos.xls';
    downloadLink.click();
    // return window.open('data:application/vnd.ms-excel;base64,' + window.btoa(table_text));
}

$(function () {
    try {
        initSocket();
    } catch {
        console.log('service not found, starting');
    }
    $("#btnPlay").click(clickPlay);
    $("#btnPause").click(clickPause);
    $("#btnStop").click(clickStop);
    $("#btnSend").click(clickSend);
    $("#btnClear").click(clickClear);
    $("#btnSave").click(clickSave);
    $("#btnDelete").click(clickDelete);
    $("#btnExport").click(clickExport);
});

