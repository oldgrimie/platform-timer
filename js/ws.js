
var ws = null;
var data = { status: 'stop', minutes: 0, seconds: 0, minutes_limit: 1000 };

function init_socket() {
    websocket_uri = 'ws://localhost:6789'
    ws = new WebSocket(websocket_uri);
    ws.onopen = function (ev) { console.log('SOCKET CONNECTED') };
    ws.onmessage = onMessage;
    ws.onclose = onClose;
}

function onClose(ev) {
    console.log('SOCKET CLOSED');
    init_socket();
}



function send_message() {
    let timer = new Date();
    let values = {
        status: 'play',
        minutes: timer.getMinutes(),
        seconds: timer.getSeconds(),
        minutes_limit: 15
    };
    ws.send(JSON.stringify(values));
}

function dataEquals(evdata) {
    return evdata.status == data.status &&
        evdata.minutes == data.minutes &&
        evdata.seconds == data.seconds;
}

function loadData(evdata){
    if (data.status == 'stop' && evdata.status == 'play'){
        console.log('Play timer');
    }
}

function onMessage(ev) {
    if (ev === undefined) return;
    // Validate current data
    if (dataEquals(ev.data)) return;
    loadData(ev.data);
    console.log(ev.data);
}


$(function () {
    try {
        init_socket();
        //console.log('connected');
    } catch {
        console.log('service not found, starting');
    }
});

