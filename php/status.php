<?php
$json_contents = file_get_contents('../data/chrono_data.json');
$json_values = json_decode($json_contents, true);
if ($json_values['status'] == 'running') {
    // Add sync data in case of page refresh
    $current_timestamp = date_create(gmdate("Y-m-d H:i:s"));
    $time_json = date_create($json_values['timestamp']);
    $interval = date_diff($time_json, $current_timestamp);
    $json_values['sync'] = array(
        'minutes' => (int) $interval->format('%i'),
        'seconds' => (int) $interval->format('%s')
    );
}
//header('Content-Type: application/json');
echo json_encode($json_values);
