<?php
require_once('websockets.php');
class SocketServerInherit extends WebSocketServer
{
    protected function process($user, $message)
    {
        echo 'user sent: ' . $message . PHP_EOL;
        foreach ($this->users as $currentUser) {
            if ($currentUser !== $user)
                $this->send($currentUser, $message);
        }
    }
    protected function connected($user)
    {
        echo 'user connected' . PHP_EOL;
    }
    protected function closed($user)
    {
        echo 'user disconnected' . PHP_EOL;
    }
}

$socketServer = new SocketServerInherit($_SERVER['SERVER_NAME'], "9000");
try {
    $socketServer->run();
} catch (Exception $e) {
    $socketServer->stdout($e->getMessage());
}
