<?php
$json_contents = file_get_contents('../data/chrono_data.json');
$json_values = json_decode($json_contents, true);


if ($json_values['status'] != 'pause') {
    $json_values['status'] = 'pause';
    file_put_contents('../data/chrono_data.json', json_encode($json_values));
}

//header('Content-Type: application/json');
echo json_encode($json_values);
