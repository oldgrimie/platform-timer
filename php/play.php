<?php
$json_contents = file_get_contents('../data/chrono_data.json');
$json_values = json_decode($json_contents, true);
if ($json_values['status'] != 'running') {
    $json_values['timestamp'] = gmdate("Y-m-d H:i:s");
}
if (isset($_POST['duration'])) {
    $json_values['duration'] = $_POST['duration'];
}
$json_values['status'] = 'running';
file_put_contents('../data/chrono_data.json', json_encode($json_values));

// header('Content-Type: application/json');
echo json_encode($json_values);
