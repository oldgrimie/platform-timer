<?php
$items = array();

$file = fopen('../data/schedule.csv', 'r');
$row = 0;
while(! feof($file)){
	
		$datos = fgetcsv($file);
	
	$item = array(
			'nro' => (string)$datos[0],
			'duracion' => $datos[1],
			'orador' => $datos[2],
			'tema' => $datos[3],
	);
	if($row != 0){
		array_push($items, $item);
	}
	$row++;
	
}
fclose($file);
echo json_encode($items);
