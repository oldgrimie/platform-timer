<?php
$json_contents = file_get_contents('../data/chrono_data.json');
$json_values = json_decode($json_contents, true);
if (isset($_POST['message'])) {
    $json_values['message'] = $_POST['message'];
}
if (isset($_POST['duration'])) {
    $json_values['duration'] = $_POST['duration'];
}
file_put_contents('../data/chrono_data.json', json_encode($json_values));
echo json_encode($json_values);
