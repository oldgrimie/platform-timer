#!/usr/bin/env python

import asyncio
import json
import logging
import websockets
import socket
import os

logging.basicConfig()

USERS = set()

VALUE = 0


def value_event():
    directory = os.path.dirname(__file__)
    with open(directory + '/estado.json', 'r') as archivo:
        json_str = archivo.read()
    return json_str


def set_values(message):
    if 'PING' in message:
        return value_event()
    directory = os.path.dirname(__file__)
    with open(directory+'/estado.json', 'w') as archivo:
        archivo.write(message)
    return message


async def counter(websocket):
    global USERS, VALUE
    try:
        # Register user
        USERS.add(websocket)
        websockets.broadcast(USERS, value_event())
        # Send current state to user
        # await websocket.send(value_event())
        # Manage state changes
        async for message in websocket:
            if message != '':
                values = set_values(message)
                websockets.broadcast(USERS, value_event())
                print(values)
                logging.info(values)
            else:
                logging.error("unsupported event: %s", message)
    finally:
        # Unregister user
        USERS.remove(websocket)
        # websockets.broadcast(USERS, users_event())


async def main(host, port):
    async with websockets.serve(counter, host, port):
        await asyncio.Future()  # run forever

if __name__ == "__main__":
    port = 6789
    hostname = socket.gethostname()
    IPAddr = socket.gethostbyname(hostname)
    print('RUNNING', hostname, IPAddr, port)
    asyncio.run(main(IPAddr, port))
